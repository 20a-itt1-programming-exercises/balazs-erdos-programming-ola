#  This is a project for programing OLA exam. The project is about unit of measure change calculator.  


##  Expected deadline for the handle in: 2020.12.20. 12:00

Tasks for the OLA:
- [x] find an idea
- [X] Plan development
    - [X] Flowchart
    - [ ] Gitlab 
- [X] find ressources
- [X] programing code
- [X] test code 
- [ ] documentation


Description of the OLA: [https://eal-itt.gitlab.io/20a-itt1-programming/20A_ITT1_PROGRAMMING_OLA16.pdf] 
